
const stateGioHang = {
    data_phone: [
        { "maSP": 1, "tenSP": "VinSmart Live", "manHinh": "AMOLED, 6.2, Full HD+", "heDieuHanh": "Android 9.0 (Pie)", "cameraTruoc": "20 MP", "cameraSau": "Chính 48 MP & Phụ 8 MP, 5 MP", "ram": "4 GB", "rom": "64 GB", "giaBan": 5700000, "hinhAnh": { "index_img": 0, "src_img": ["./img/vsphone.jpg", "https://img.nhabanhang.com/sp/f/295914/dien-thoai-vsmart-live-ram-6gb-64gb-nguyen-seal-chua-kich-hoat-724.jpg"] } },
        { "maSP": 2, "tenSP": "Meizu 16Xs", "manHinh": "AMOLED, FHD+ 2232 x 1080 pixels", "heDieuHanh": "Android 9.0 (Pie); Flyme", "cameraTruoc": "20 MP", "cameraSau": "Chính 48 MP & Phụ 8 MP, 5 MP", "ram": "4 GB", "rom": "64 GB", "giaBan": 7600000, "hinhAnh": { "index_img": 0, "src_img": ["./img/meizuphone.jpg", "https://www.meizuworld.com/media/catalog/product/cache/c1f9f9e9e10e768abf6e60f272b02b02/m/e/meizu_16xs_white_1__1.png"] } },
        { "maSP": 3, "tenSP": "Iphone XS Max", "manHinh": "OLED, 6.5, 1242 x 2688 Pixels", "heDieuHanh": "iOS 12", "cameraSau": "Chính 12 MP & Phụ 12 MP", "cameraTruoc": "7 MP", "ram": "4 GB", "rom": "64 GB", "giaBan": 27000000, "hinhAnh": { "index_img": 0, "src_img": ["./img/applephone.jpg", "https://www.queenmobile.info/wp-content/uploads/2021/01/iphone-x-white-700x700-1.jpg"] } }
    ],
    gioHang: [],
}

export const gioHangReducer = (state = stateGioHang, action) => {
    switch (action.type) {
        case 'THEM_GIO_HANG': {
            let clonegioHang = [...state.gioHang];

            let index = clonegioHang.findIndex((item) => {
                return item.maSP == action.spGioHang.maSP;
            });

            if (index == -1) {
                let clonespGioHang = { ...action.spGioHang, soLuong: 1 };
                clonegioHang.push(clonespGioHang);
            }

            else {
                clonegioHang[index].soLuong++;
            }


            return { ...state, gioHang: clonegioHang }
        }
        case 'XOA_GIO_HANG': {
            let clonegioHang = [...state.gioHang];
            const filteredItem = clonegioHang.filter((item) => {
                return item.maSP !== action.maSP
            });

            return { ...state, gioHang: filteredItem };
        }

        case 'THAY_DOI_SO_LUONG': {
            let clonegioHang = [...state.gioHang];
            const viTri = clonegioHang.findIndex((item) => {
                return item.maSP === action.maSP
            });

            clonegioHang[viTri].soLuong += action.soLuong;

            return { ...state, gioHang: clonegioHang }
        }
        default:
            return state;
    }

}

