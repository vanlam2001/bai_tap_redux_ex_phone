import { combineReducers } from "redux"
import { detailReducer } from "./DetailReducer"
import { gioHangReducer } from "./GioHangReducer"

export const rootReducer = combineReducers({
    gioHangReducer,
    detailReducer
})