let stateDetail = {
    detail: {
        "maSP": 1, "tenSP": "VinSmart Live", "manHinh": "AMOLED, 6.2, Full HD+", "heDieuHanh": "Android 9.0 (Pie)", "cameraTruoc": "20 MP", "cameraSau": "Chính 48 MP & Phụ 8 MP, 5 MP", "ram": "4 GB", "rom": "64 GB", "giaBan": 5700000, "hinhAnh": { "index_img": 0, "src_img": ["./img/vsphone.jpg", "https://img.nhabanhang.com/sp/f/295914/dien-thoai-vsmart-live-ram-6gb-64gb-nguyen-seal-chua-kich-hoat-724.jpg"] }
    }
}

export const detailReducer = (state = stateDetail, action) => {
    switch (action.type) {
        case 'XEM_CHI_TIET': {
            state.detail = action.spdetailed;
            return { ...state };
        }
    }
    return { ...state }
}