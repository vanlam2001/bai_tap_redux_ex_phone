import React, { Component } from 'react'
import { connect } from 'react-redux';
import { formatter } from './ForMatter';



class PhoneCart extends Component {

    renderPhoneCartWithMap = () => {
        return this.props.gioHang.map((phone) => {

            let { hinhAnh } = phone;
            let img = hinhAnh.src_img;
            let imgIndex = hinhAnh.index_img;

            return (
                <tr>
                    <td>{phone.maSP}</td>
                    <td>{phone.tenSP}</td>
                    <td>{formatter.format(phone.giaBan)}</td>
                    <td><img width={50} src={img[imgIndex]} /></td>
                    <td><button onClick={() => {
                        this.props.thayDoiSoLuong(phone.maSP, -1)
                    }} className='btn btn-outline-primary mr-2'>-</button>
                        {phone.soLuong} <button onClick={() => {
                            this.props.thayDoiSoLuong(phone.maSP, +1)
                        }} className='btn btn-outline-primary ml-2'>+</button>
                    </td>
                    <td>{formatter.format(phone.giaBan * phone.soLuong)}</td>
                    <td><button onClick={() => {
                        this.props.xoaGioHang(phone.maSP)
                    }} className='btn btn-danger'>Xóa</button></td>
                </tr>
            )
        })
    }
    render() {
        return (
            <div>
                <div className="container p-5">
                    <h2 className='text-center'>Phone Cart</h2>
                    <table className='table table-striped table-inverse mt-5'>
                        <thead className='thead-inverse'>
                            <tr>
                                <th>Mã sp</th>
                                <th>Tên sp</th>
                                <th>Giá Bán</th>
                                <th>Hình Ảnh</th>
                                <th>Số Lượng</th>
                                <th>Tổng tiền</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderPhoneCartWithMap()}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { gioHang: state.gioHangReducer.gioHang }
}

const mapDispatchToProps = (dispatch) => {
    return {
        xoaGioHang: (maSP) => {
            const action = {
                type: 'XOA_GIO_HANG',
                maSP: maSP
            }
            dispatch(action)
        },

        thayDoiSoLuong: (maSP, soLuong) => {
            const action = {
                type: 'THAY_DOI_SO_LUONG',
                maSP: maSP,
                soLuong: soLuong
            }
            dispatch(action)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhoneCart);
