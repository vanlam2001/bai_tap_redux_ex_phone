import React, { Component } from 'react'
import { connect } from 'react-redux';
import { formatter } from './ForMatter';

class ItemPhone extends Component {
    RenderPhoneWithMap = () => {
        return this.props.data_phone.map((phone) => {
            let { hinhAnh } = phone;
            let img = hinhAnh.src_img;
            let imgIndex = hinhAnh.index_img;
            return (
                <div className="col-4">
                    <div className="card  h-100 text-center" style={{ width: "20rem" }}>
                        <img className='card-img-top' src={img[imgIndex]} />
                        <div className="card-body">
                            <h4 className='card-title'>{phone.tenSP}</h4>
                            <p className='card-text'>{formatter.format(phone.giaBan)}</p>
                            <div className="mb-3 text-left">
                                <span></span>
                                <span></span>
                            </div>
                            <div className="d-flex justify-content-between">
                                <button onClick={() => {
                                    this.props.xemChiTiet(phone)
                                }} className='btn btn-danger'>Xem chi tiết</button>
                                <button onClick={() => {
                                    this.props.themGioHang(phone)
                                }} className='btn btn-success'>Thêm vào giỏ hàng</button>
                            </div>
                        </div>
                    </div>
                </div>
            )
        })
    }
    render() {
        console.log(this.props.data_phone);
        return (
            <div>
                <div className="row">
                    {this.RenderPhoneWithMap()}
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        themGioHang: (sanPham) => {
            const spGioHang = {
                maSP: sanPham.maSP,
                tenSP: sanPham.tenSP,
                giaBan: sanPham.giaBan,
                hinhAnh: sanPham.hinhAnh,

            }
            const action = {
                type: 'THEM_GIO_HANG',
                spGioHang: spGioHang
            }
            dispatch(action);
        },
        xemChiTiet: (sanPham) => {
            const action = {
                type: 'XEM_CHI_TIET',
                spdetailed: sanPham
            }
            dispatch(action);
        }
    }
}

const mapStateToProps = (state) => {
    return {
        data_phone: state.gioHangReducer.data_phone
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemPhone);

